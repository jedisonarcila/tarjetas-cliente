# tarjetas-cliente

Repositorio prueba tecnica ibm el cual tiene como objetivo:

construir una aplicación Web Responsive con las siguientes páginas:

•Mostrar una lista de clientes con sus datos y sus tarjetas
•Al hacer click sobre un registro, mostrar el historial de consumos
•Una página con los Asesores con sus datos y su especialidad

Estas instrucciones le permitirán obtener una copia del proyecto y su respectiva configuracion para montaje del entorno de desarrollo
Prerrequisitos

Requerimientos de software

* Git
* Java JDK 1.8
* Eclipse Neon o superior
* Gradle
* Node 8 o superior
* Angular Cli


Frameworks utilizados

* Sprint Boot
* Sprint Framework
* JPA
* H2 (Base de datos embebida)

Instalando

Paso a paso configuración entorno
Dirigirse al directorio DOCUMENTACION y seguir los pasos de las guias de configuración deacuerdo al proyecto que quiera configurar (Gradle)

    /DOCUMENACION/GUIA CONFIGURACIÓN PROYECTO JAVA.pdf   
	/GUIA CONFIGURACIÓN PROYECTO ANGULAR.pdf  

Change log

Version 0.1: Implementación simulación gestión tarjetas cliente.
