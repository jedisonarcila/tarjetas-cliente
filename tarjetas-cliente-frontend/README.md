# AvisosFront

Este proyecto fue generado con Angular Cli version 7.2.0

## Dependencias:

Este proyecto depende del proyecto backend que se encuentra en los siguientes repositorios: 

## Requerimientos de software

* Node js 8.x
* Visual estudio code

## Servidor local

Pre-requisito:
- Tener proyecto backend corriendo en el puerto 9550
- Configurar en el archivo host el siguiente dns con la ip 127.0.0.1 localhost.suramericana.com.co
- Estar conectado a la vpn de suramericana

Instalando:
- Descargar el proyecto del repositorio y abrirlo con el IDE recomendado para angular (Visual Studio Code)
- Nos dirigimos al archivo environment.ts y verificamos que la propiedad urlBase contenga el siguiente valor: urlBase: './'
- Desde la terminal debemos ejecutar el comando npm install
- Desde la terminal debemos ejecutar el comando npm start
- Abrimos el proyecto con la siguiente url: http://localhost.suramericana.com.co:4200/

## Servidor sura desarrollo

Para instalar en ambiente de sura debemos tener el proyecto del backend configurado con la url de git de sura

En el proyecto del front nos dirigimos al archivo environment.ts y verificamos que la propiedad urlBase contenga el siguiente valor: urlBase: '/avisosmantenimiento/'

En el proyecto del front desde la terminal ejecutamos el comando npm run buildpdn, dicho comando se encarga de generar los fuentes minificados del front en el proyecto del backend.

Despues vamos al control de versiones del proyecto del backend y subimos los cambios generados a la rama de sura, con esto el proyecto se instalara automaticamente ya que se encuentra configurado con jenkins:

## Tecnologias utilizadas:

- Angular 7

## Change log

Version 0.1: Implementacion inicial del proyecto ver historias de usuario: https://pratech.atlassian.net/secure/RapidBoard.jspa?rapidView=368&projectKey=SPGDAM&view=planning&selectedIssue=SPGDAM-146