import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { MensajeriaService } from '../../modal-mensajes/service/mensajeria.service';
import { AppService } from '../../services/app.service';
import { ClienteConstantes, BANDEJAS } from '../ClienteConstantes';
import { Router } from '@angular/router';

@Component({
  selector: 'lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
/**Componente para gestionar la lista de clientes */
export class ListaClientesComponent implements OnInit {

  listaClientes: any[] = [];
  listaFilasFiltradas: any[] = [];
  modelColumnas: any[] = [];
  listaColumnas: any = BANDEJAS;
  /**Constructor del componente */
  constructor(private mensajeService: MensajeriaService,
    private clienteService: ClienteService,
    private appService: AppService,
    private router: Router) { }

  ngOnInit() {
    this.construirTabla();
    this.consultarClientes();
  }
  /**Metodo que invoca el servicio que retorna la lista de clientes */
  consultarClientes() {
    this.appService.mostrarLoading();
    this.clienteService.consultarClientes().then((response) => {
      if (response !== null && response !== undefined && response !== '') {
        this.listaClientes = response;
        this.listaFilasFiltradas = response;
      }
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', ClienteConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
  /**Metodo que construye las columnas que va a contener la tabla que muestra la lista de clientes */
  construirTabla() {
    this.modelColumnas = [];
    if (this.listaColumnas['bandeja-clientes'] && this.listaColumnas['bandeja-clientes'].columnas) {
      for (const i in this.listaColumnas['bandeja-clientes']['columnas']) {
        if (this.listaColumnas['bandeja-clientes']['columnas'][i]) {
          this.modelColumnas.push(this.listaColumnas['bandeja-clientes']['columnas'][i]);
        }
      }
    }
  }
  /**Metodo para filtrado */
  filtrarRegistros(columna: any, value: any) {
    let valor = value;
    this.listaFilasFiltradas = this.listaClientes.filter(obj => this.validarFiltro(obj, columna, valor));
  }
  /**Metodo valida si la cadena ingresada en el filtro esta contenida en la lista de clientes por columna */
  validarFiltro(obj, campo, value) {
    return obj[campo].toUpperCase().includes(value.toUpperCase());
  }
  /**Obtiene el valor de una propiedad */
  format(fila, columna) {
    const temp = fila[columna];
    return temp === undefined ? '' : temp;
  }
  /**Metodo que redirige a la pantalla de listas del cliente */
  irGestionCliente() {
    this.router.navigate(['/detalle-cliente/0']);
  }
  /**Metodo que construye la url para la pantalla detalle cliente */
  construirUrl(fila) {
    return `/detalle-cliente/` + fila.id;
  }
  /**Metodo que invoca el servicio para eliminar un registro */
  eliminarRegistro(fila) {
    this.appService.mostrarLoading();
    this.clienteService.eliminarClientes(fila.id).then((response) => {
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', ClienteConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
}


