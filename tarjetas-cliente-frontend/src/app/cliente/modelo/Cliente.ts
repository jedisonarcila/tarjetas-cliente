export class Cliente {
    id: Number = 0;
    nombre: string = '';
    direccion: string = '';
    ciudad: string = '';
    telefono: string = '';
    numeroTarjetas: Number = 0;
    tarjetas: any[] = [];
}
