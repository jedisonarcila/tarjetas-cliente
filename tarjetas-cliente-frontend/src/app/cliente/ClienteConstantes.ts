import { environment } from '../../environments/environment';
/**Clase de constantes para el componente del cliente */
export class ClienteConstantes {

    public static ambiente_desarrollo = environment.develop;
    public static urlBase = environment.urlBase;

    public static get getUrl() {
        return {
            urlBase: this.urlBase,
            url_consultar_clientes: this.urlBase + 'api/clientes',
            url_agregar_cliente: this.urlBase + 'api/clientes',
            url_actualizar_cliente: this.urlBase + 'api/clientes',
            url_eliminar_cliente: this.urlBase + 'api/clientes'
        };
    }

    public static get getConstantes() {
        return {
            msjError: 'Ocurrio un error, favor comunicarse con el administrador del sistema.',
            msjExito: 'El registro fue gestionado con exito.'
        };
    }
}

export const BANDEJAS = {
    'bandeja-clientes': {
        'columnas': {
            'nombre': {
                'titulo': 'NOMBRE DEL CLIENTE',
                'nombre': 'nombre',
                'tipo': 'texto'
            },
            'direccion': {
                'titulo': 'DIRECCIÓN',
                'nombre': 'direccion',
                'tipo': 'texto'
            },
            'ciudad': {
                'titulo': 'CIUDAD',
                'nombre': 'General.UbicacionTxt',
                'tipo': 'texto'
            },
            'telefono': {
                'titulo': 'TELEFONO',
                'nombre': 'telefono',
                'tipo': 'texto'
            },
            'numero tarjetas': {
                'titulo': 'No TARJETAS ASOCIADAS',
                'nombre': 'numeroTarjetas',
                'tipo': 'texto',
                'accion': 'delete'
            }
        }
    }
};

