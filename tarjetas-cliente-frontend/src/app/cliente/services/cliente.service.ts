import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ClienteConstantes } from '../ClienteConstantes';

@Injectable({
    providedIn: 'root'
})
/**Servicio para invocar las funcionalidades de backend */
export class ClienteService {

    constructor(private http: HttpClient) { }

    /**Metodo que consulta todos los clientes */
    public consultarClientes(): Promise<any> {
        const url = ClienteConstantes.getUrl.url_consultar_clientes;
        return this.http.get(`${url}`, this.getOptions()).toPromise();
    }
    /**Metodo que consulta un cliente por id */
    public consultarCliente(id): Promise<any> {
        const url = ClienteConstantes.getUrl.url_consultar_clientes;
        return this.http.get(`${url}/${id}`, this.getOptions()).toPromise();
    }
    /**Metodo que adiciona un cliente */
    public agregarCliente(data: any): Promise<any> {
        const url = ClienteConstantes.getUrl.url_agregar_cliente;
        return this.http.post(url, data, this.getOptions()).toPromise();
    }
    /**Metodo que actualiza un cliente por id */
    public actualizarCliente(data: any): Promise<any> {
        const url = ClienteConstantes.getUrl.url_actualizar_cliente;
        return this.http.put(url, data, this.getOptions()).toPromise();
    }
    /**Metodo que elimina un cliente por id */
    public eliminarClientes(id): Promise<any> {
        const url = ClienteConstantes.getUrl.url_eliminar_cliente;
        return this.http.delete(`${url}/${id}`, this.getOptions()).toPromise();
    }
    /**Opciones del encabezado de la petición */
    private getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    }
}
