import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { MensajeriaService } from '../../modal-mensajes/service/mensajeria.service';
import { AppService } from '../../services/app.service';
import { ClienteConstantes } from '../ClienteConstantes';
import { ActivatedRoute } from '@angular/router';
import { Cliente } from '../modelo/Cliente';

@Component({
  selector: 'app-detalle-cliente',
  templateUrl: './detalle-cliente.component.html',
  styleUrls: ['./detalle-cliente.component.css']
})
export class DetalleClienteComponent implements OnInit {

  id;
  cliente = new Cliente();
  accion = 'Guardar';

  /**Constructor del componente */
  constructor(private mensajeService: MensajeriaService,
    private clienteService: ClienteService,
    private appService: AppService,
    private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if (params['id'] != null && params['id'] != 0) {
        this.id = params['id'];
        this.accion = 'Editar';
      } else {
        this.accion = 'Guardar';
      }
    });
  }

  /**Metodo que se ejecuta al cargar el componente */
  ngOnInit() {
    if (this.accion == 'Editar') {
      this.consultarCliente();
    }
  }

  /**Metodo que consulta un cliente por id */
  consultarCliente() {
    this.appService.mostrarLoading();
    this.clienteService.consultarCliente(this.id).then((response) => {
      if (response !== null && response !== undefined && response !== '') {
        this.cliente = response;
      }
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', ClienteConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }

  /**Metodo que envia la petición para almacenar un cliente */
  enviar() {
    this.appService.mostrarLoading();
    this.clienteService.agregarCliente(this.cliente).then((response) => {
      this.mensajeService.openModal('fa-check-circle', ClienteConstantes.getConstantes.msjExito, 'alerta');
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', ClienteConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
}
