import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppService } from '../services/app.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
/**Componente para renderizar el encabezado de la aplicación*/
export class HeaderComponent implements OnInit, OnDestroy {
  /**
   * Url de la imagen de perfil
   */
  photo: String = 'assets/img/userPhoto.png';

  showOptionsUser = false;

  openMenu: Boolean = false;

  /**
   * Indica si el menú esta visible o no
   */
  menuActive = false;

  /**
   * Indica si el menú de usuario esta visiable o no
   */
  userMenuActive = false;

  /**
   * Subscription para obtener el estado del sidebar
   */
  private sidebarSuscription: Subscription;

  /**
   * Constructor del componente
   * @paramidentityService
   * @paramappService
   */
  constructor(protected appService: AppService) {
  }

  /**
   * Inicialización, se carga el estado del sidebar y el usuario
   */
  ngOnInit(): void { }

  /**
   * Se libera recursos
   */
  ngOnDestroy(): void {
    this.sidebarSuscription.unsubscribe();
  }

  errorPhoto() {
    this.photo = 'assets/img/userPhoto.png';
  }

  resMobile() {
    if (!this.openMenu) {
      this.openMenu = true;
      this.appService.mostrarMenu();
    } else {
      this.openMenu = false;
      this.appService.ocultarMenu();
    }
  }

  showOptions($event) {
    this.showOptionsUser = !this.showOptionsUser;
  }

  // gen_seus
}
