import { environment } from '../environments/environment';

export class AppConstantes {

    public static ambiente_desarrollo = environment.develop;
    public static urlBase = environment.urlBase;

    public static get getUrl() {
        return {
            urlBase: this.urlBase,
            url_consultar_clientes: this.urlBase + 'api/clientes',
            url_consultar_catalogos: this.urlBase + 'api/catalogo/search',
            url_actualizar_aviso: this.urlBase + 'api/aviso/update'
        };
    }
}
