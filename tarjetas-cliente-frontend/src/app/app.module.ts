import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);

// Pluging's
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgSelectModule } from '@ng-select/ng-select';
import { TableModule } from 'primeng/table';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { CheckboxModule } from 'primeng/checkbox';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgxMaskModule } from 'ngx-mask';
import { TextMaskModule } from 'angular2-text-mask';


// RouteConfig
import { AppRoutingModule } from './app.routing';

// Components
import { AppComponent } from './app.component';
import { ListaClientesComponent } from './cliente/lista-clientes/lista-clientes.component';
import { DetalleClienteComponent } from './cliente/detalle-cliente/detalle-cliente.component';
import { AlfabeticoDirective } from './directivas/alfabetico.directive';
import { NumerosDirective } from './directivas/numeros.directive';
import { EmailValidator } from './directivas/emailvalidator.directive';

// Services
import { HeaderComponent } from './header/header.component';
import { LoadingComponent } from './loading/loading.component';
import { AppService } from './services/app.service';
import { MensajeriaComponent } from './modal-mensajes/mensajes.component';
import { MensajeriaService } from './modal-mensajes/service/mensajeria.service';
import { ListaTarjetasComponent } from './tarjetas/lista-tarjetas/lista-tarjetas.component';
import { DetalleTarjetaComponent } from './tarjetas/detalle-tarjeta/detalle-tarjeta.component';


@NgModule({
  declarations: [
    AppComponent,
    MensajeriaComponent,
    ListaClientesComponent,
    ListaTarjetasComponent,
    DetalleClienteComponent,
    DetalleTarjetaComponent,
    AlfabeticoDirective,
    NumerosDirective,
    EmailValidator,
    HeaderComponent,
    LoadingComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgSelectModule,
    TableModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    CheckboxModule,
    PopoverModule.forRoot(),
    CurrencyMaskModule,
    TextMaskModule,
    TimepickerModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  providers: [
    AppService,
    MensajeriaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
