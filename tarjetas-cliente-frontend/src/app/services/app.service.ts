import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

/**Servicio para pasar mensajes de alertas entre componentes */
@Injectable()
export class AppService {

    constructor() { }

    // Observable string sources
    private isOpenMenu = new Subject<boolean>();
    private isLoading = new Subject<boolean>();
    private isLoadingModal = new Subject<boolean>();

    // Observable string streams
    isOpenMenu$ = this.isOpenMenu.asObservable();
    isLoading$ = this.isLoading.asObservable();
    isLoadingModal$ = this.isLoadingModal.asObservable();

    // Service message commands
    mostrarMenu() {
        this.isOpenMenu.next(true);
    }
    ocultarMenu() {
        this.isOpenMenu.next(false);
    }
    // Service message commands
    mostrarLoading() {
        this.isLoading.next(true);
    }
    ocultarLoading() {
        this.isLoading.next(false);
    }
    // Service message commands
    mostrarLoadingModal() {
        this.isLoadingModal.next(true);
    }
    ocultarLoadingModal() {
        this.isLoadingModal.next(false);
    }
}
