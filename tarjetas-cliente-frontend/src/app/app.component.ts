import { Component, TemplateRef } from '@angular/core';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AppService } from './services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  locale = 'es';
  modalRef: BsModalRef;
  isOpenMenu: boolean;
  isLoading: boolean = true;

  constructor(private modalService: BsModalService,
    private localeService: BsLocaleService,
    protected appService: AppService) {
    this.appService.isOpenMenu$.subscribe(
      openMenu => this.isOpenMenu = openMenu
    );

    this.appService.isLoading$.subscribe(
      loading => this.isLoading = loading
    );

    this.localeService.use(this.locale);
  }

  openModalAvisosTipologia() {
    console.log("Redirect a pantalla clientes");
  }

  errorMsg(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-msg-alert' });
  }

}
