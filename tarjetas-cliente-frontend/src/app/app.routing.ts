import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MensajeriaComponent } from './modal-mensajes/mensajes.component';
import { ListaClientesComponent } from './cliente/lista-clientes/lista-clientes.component';
import { DetalleClienteComponent } from './cliente/detalle-cliente/detalle-cliente.component';
import { ListaTarjetasComponent } from './tarjetas/lista-tarjetas/lista-tarjetas.component';
import { DetalleTarjetaComponent } from './tarjetas/detalle-tarjeta/detalle-tarjeta.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'lista-clientes', pathMatch: 'full' },
    { path: 'lista-clientes', component: ListaClientesComponent },
    { path: 'detalle-cliente/:id', component: DetalleClienteComponent },
    { path: 'lista-tarjetas', component: ListaTarjetasComponent },
    { path: 'detalle-tarjeta/:id', component: DetalleTarjetaComponent },
    { path: 'mensajes', component: MensajeriaComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: false }),
        CommonModule
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})

export class AppRoutingModule { }

// export const appRoutingProviders: any[] = [];
// export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
