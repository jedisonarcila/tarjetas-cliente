import { environment } from '../../environments/environment';

export class TarjetaConstantes {

    public static ambiente_desarrollo = environment.develop;
    public static urlBase = environment.urlBase;
    public static obligatorios = ['numeroTarjeta'];
    public static colsHeader = [
        { field: 'fechaConsumo', header: 'Fecha Consumo' },
        { field: 'descripcion', header: 'Descripción' },
        { field: 'monto', header: 'monto' },
        { field: 'delete', header: '' }
    ];
    public static bsConfig = {
        dateInputFormat: 'DD-MM-YYYY',
        rangeInputFormat: 'DD-MM-YYYY',
        containerClass: 'theme-dark-blue',
    };

    public static get getUrl() {
        return {
            urlBase: this.urlBase,
            url_consultar_tarjetas: this.urlBase + 'api/tarjetas',
            url_actualizar_tarjeta: this.urlBase + 'api/tarjetas',
            url_agregar_tarjetas: this.urlBase + 'api/tarjetas',
            url_eliminar_tarjeta: this.urlBase + 'api/tarjetas'
        };
    }

    public static get getConstantes() {
        return {
            msjError: 'Ocurrio un error, favor comunicarse con el administrador del sistema.',
            msjExito: 'El registro fue gestionado con exito.',
            colsHeader: this.colsHeader
        };
    }
}

export const BANDEJA = {
    'bandeja-tarjetas': {
        'columnas': {
            'numeroTarjeta': {
                'titulo': 'No TARJETA',
                'nombre': 'numeroTarjeta',
                'tipo': 'texto'
            },
            'ccv': {
                'titulo': 'CCV',
                'nombre': 'ccv',
                'tipo': 'texto'
            },
            'tipoTarjeta': {
                'titulo': 'TIPO TARJETA',
                'nombre': 'tipoTarjeta',
                'tipo': 'texto',
                'accion': 'delete'
            }
        }
    }
};
