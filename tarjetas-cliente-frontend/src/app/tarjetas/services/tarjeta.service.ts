import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TarjetaConstantes } from '../TarjetaConstantes';

@Injectable({
    providedIn: 'root'
})
/**Servicio para invocar las funcionalidades del backend */
export class TarjetaService {

    constructor(private http: HttpClient) { }
    /**Metodo que consulta todas las tarjetas */
    public consultarTarjetas(): Promise<any> {
        const url = TarjetaConstantes.getUrl.url_consultar_tarjetas;
        return this.http.get(`${url}`, this.getOptions()).toPromise();
    }
    /**Metodo que consulta una tarjeta por id */
    public consultarTarjeta(id): Promise<any> {
        const url = TarjetaConstantes.getUrl.url_consultar_tarjetas;
        return this.http.get(`${url}/${id}`, this.getOptions()).toPromise();
    }
    /**Metodo que almacena una tarjeta */
    public agregarTarjeta(data: any): Promise<any> {
        const url = TarjetaConstantes.getUrl.url_agregar_tarjetas;
        return this.http.post(url, data, this.getOptions()).toPromise();
    }
    /**Metodo que actualiza una tarjeta */
    public actualizarTarjeta(data: any): Promise<any> {
        const url = TarjetaConstantes.getUrl.url_actualizar_tarjeta;
        return this.http.put(url, data, this.getOptions()).toPromise();
    }
    /**Metodo que elimina una tarjeta */
    public eliminarTarjeta(id): Promise<any> {
        const url = TarjetaConstantes.getUrl.url_eliminar_tarjeta;
        return this.http.delete(`${url}/${id}`, this.getOptions()).toPromise();
    }
    /**Opciones del encabezado de la petición */
    private getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    }
}
