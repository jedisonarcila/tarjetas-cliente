import { Component, OnInit } from '@angular/core';
import { TarjetaService } from '../services/tarjeta.service';
import { MensajeriaService } from '../../modal-mensajes/service/mensajeria.service';
import { AppService } from '../../services/app.service';
import { TarjetaConstantes, BANDEJA } from '../TarjetaConstantes';
import { Router } from '@angular/router';

@Component({
  selector: 'lista-tarjetas',
  templateUrl: './lista-tarjetas.component.html',
  styleUrls: ['./lista-tarjetas.component.css']
})
/**Componente para listar las tarjetas */
export class ListaTarjetasComponent implements OnInit {

  listaTarjetas: any[] = [];
  listaFilasFiltradas: any[] = [];
  modelColumnas: any[] = [];
  listaColumnas: any = BANDEJA;
  /**Constructor del componente */
  constructor(private mensajeService: MensajeriaService,
    private tarjetaService: TarjetaService,
    private appService: AppService,
    private router: Router) { }
  /**Metodo que se ejecuta cuando se carga el componente */
  ngOnInit() {
    this.construirTabla();
    this.consultarTarjetas();
  }
  /**Metodo para consultar todas las tarjetas */
  consultarTarjetas() {
    this.appService.mostrarLoading();
    this.tarjetaService.consultarTarjetas().then((response) => {
      if (response !== null && response !== undefined && response !== '') {
        this.listaTarjetas = response;
        this.listaFilasFiltradas = response;
      }
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', TarjetaConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
  /**Metodo que construye las columnas de la tabla en donde se va a mostrar las tarjetas */
  construirTabla() {
    this.modelColumnas = [];
    if (this.listaColumnas['bandeja-tarjetas'] && this.listaColumnas['bandeja-tarjetas'].columnas) {
      for (const i in this.listaColumnas['bandeja-tarjetas']['columnas']) {
        if (this.listaColumnas['bandeja-tarjetas']['columnas'][i]) {
          this.modelColumnas.push(this.listaColumnas['bandeja-tarjetas']['columnas'][i]);
        }
      }
    }
  }
  /**Metodo para filtrar las columnas */
  filtrarRegistros(columna: any, value: any) {
    let valor = value;
    const col = columna.split('.');
    const campo = col[1];
    this.listaFilasFiltradas = this.listaTarjetas.filter(obj => this.validarFiltro(obj, campo, valor));
  }
  /**Metodo para validar filtro */
  validarFiltro(obj, campo, value) {
    return obj[campo].toUpperCase().includes(value.toUpperCase());
  }
  /**Metodo que obtiene el valor del registro por columna */
  format(fila, columna) {
    const temp = fila[columna];
    return temp === undefined ? '' : temp;
  }
  /**Metodo que redirecciona a la pagina principal */
  irGestionCliente() {
    this.router.navigate(['/detalle-tarjeta/0']);
  }
  /**Metodo que construye la url para ir al detalle de las tarjetas */
  construirUrl(fila) {
    return `/detalle-tarjeta/` + fila.id;
  }
  /**Elimina un registro por id */
  eliminarRegistro(fila) {
    this.appService.mostrarLoading();
    this.tarjetaService.eliminarTarjeta(fila.id).then((response) => {
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', TarjetaConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
}


