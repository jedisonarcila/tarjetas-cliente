export class Tarjeta {
    id: Number = 0;
    numeroTarjeta: string = '0000-0000-0000-0000';
    ccv: Number = 0;
    tipoTarjeta: string = '';
    consumos: any[] = [];
}
