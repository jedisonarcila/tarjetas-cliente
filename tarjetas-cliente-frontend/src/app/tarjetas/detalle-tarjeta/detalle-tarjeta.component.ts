import { Component, OnInit } from '@angular/core';
import { TarjetaService } from '../services/tarjeta.service';
import { MensajeriaService } from '../../modal-mensajes/service/mensajeria.service';
import { AppService } from '../../services/app.service';
import { TarjetaConstantes } from '../TarjetaConstantes';
import { ActivatedRoute } from '@angular/router';
import { Tarjeta } from '../modelo/Tarjeta';
import { Consumo } from '../modelo/Consumo';

@Component({
  selector: 'app-detalle-tarjeta',
  templateUrl: './detalle-tarjeta.component.html',
  styleUrls: ['./detalle-tarjeta.component.css']
})
/**Componente detalle de las tarjetas */
export class DetalleTarjetaComponent implements OnInit {

  id;
  tarjeta = new Tarjeta();
  consumo = new Consumo();
  accion = 'Guardar';
  colsHeader = TarjetaConstantes.colsHeader;
  bsConfig = TarjetaConstantes.bsConfig;
  obligatorios = TarjetaConstantes.obligatorios;
  objvalidacion: {};
  /**Metodo que constructor de la clase */
  constructor(private mensajeService: MensajeriaService,
    private tarjetaService: TarjetaService,
    private appService: AppService,
    private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if (params['id'] != null && params['id'] != 0) {
        this.id = params['id'];
        this.accion = 'Editar';
      } else {
        this.accion = 'Guardar';
      }
    });
  }
  /**Metodo que se ejecuta cuando se carga la app */
  ngOnInit() {
    if (this.accion == 'Editar') {
      this.consultarTarjeta();
    }
  }
  /**Mascara personalizada para el campo número de tarjeta */
  getMask(): {
    mask: Array<string | RegExp>;
    keepCharPositions: boolean;
  } {
    return {
      mask: [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
      keepCharPositions: true
    };
  }
  /**Metodo que consulta una tarjeta por id */
  consultarTarjeta() {
    this.appService.mostrarLoading();
    this.tarjetaService.consultarTarjeta(this.id).then((response) => {
      if (response !== null && response !== undefined && response !== '') {
        this.tarjeta = response;
      }
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', TarjetaConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
  /**Metodo que envia la petición para guardar un registro */
  enviar() {
    this.appService.mostrarLoading();
    this.tarjetaService.agregarTarjeta(this.tarjeta).then((response) => {
      this.mensajeService.openModal('fa-check-circle', TarjetaConstantes.getConstantes.msjExito, 'alerta');
      this.appService.ocultarLoading();
    }).catch((error) => {
      this.mensajeService.openModal('fa-times-circle', TarjetaConstantes.getConstantes.msjError, 'alerta');
      this.appService.ocultarLoading();
    });
  }
  /**Metodo para validar la obligatoriedad de los campos */
  validarObligatorios() {
    this.obligatorios.forEach(element => {
      if (!this.tarjeta[element] || this.tarjeta[element] == '') {
        this.objvalidacion[element] = true;
      }
    });
  }
  /**Elimina un elemento del array de consumos el cual se enviara a actualizar */
  eliminarConsumo(consumo) {
    let index = this.tarjeta.consumos.findIndex(con => con.id === consumo.id);
    this.tarjeta.consumos.splice(index, 1);
  }
  /**Agrega un elemento del array de consumos el cual se enviara a actualizar */
  agregarConsumo() {
    if (this.consumo.descripcionv && this.consumo.monto && this.consumo.fechaConsumo) {
      this.tarjeta.consumos.push(this.consumo);
    }
  }
}

