import { Component, OnInit } from '@angular/core';
import { AppConstantes } from '../app-contantes';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
/**Componente loading*/
export class LoadingComponent implements OnInit {

  urlBase = AppConstantes.getUrl.urlBase;

  constructor() { }

  ngOnInit() {
  }

}
