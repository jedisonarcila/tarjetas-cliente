import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MensajeriaComponent } from '../mensajes.component';

@Injectable()
export class MensajeriaService {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  constructor(private modalService: BsModalService) { }

  openModal(className, message, tipo) {
    const initialState = { class: className, message: message, tipo: tipo };
    let tamanoModal = 'modal-msg-alert';
    if (tipo == 'alerta') {
      tamanoModal = 'modal-msg-alert';
    } else {
      tamanoModal = 'modal-refuse';
    }
    this.modalRef = this.modalService.show(MensajeriaComponent, Object.assign({}, { class: tamanoModal, initialState }, this.config));
    this.modalRef.content.onClose = (myData) => {
      this.modalRef.hide();
    };
  }
}
