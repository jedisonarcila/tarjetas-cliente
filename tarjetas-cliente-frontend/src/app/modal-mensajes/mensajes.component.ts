import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styleUrls: ['./mensajes.component.css']
})
export class MensajeriaComponent {

  @Input() class: string;
  @Input() message: string;
  @Input() tipo: string;

  constructor(private _bsModalRef: BsModalRef, private router: Router) { }

  public onCancel(): void {
    this._bsModalRef.hide();
  }

  irPantallaInicial() {
    this.router.navigate([`/lista-clientes`]);
    this._bsModalRef.hide();
  }
}
