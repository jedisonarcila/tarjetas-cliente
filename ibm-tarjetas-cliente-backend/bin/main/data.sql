insert into Asesor (id,nombre,especialidad)values(1, 'alex','raidentrance');
insert into Asesor (id,nombre,especialidad)values(2, 'juan','juanito dubalin');
insert into Asesor (id,nombre,especialidad)values(3, 'pedro','mascara sagrada');

insert into Cliente (id,nombre,direccion,ciudad,telefono)values(1, 'Jhon Edison', 'Cra 5', 'MEDLLIN', 3194973736);
insert into Cliente (id,nombre,direccion,ciudad,telefono)values(2, 'Sebastian Arcila', 'Cra 3', 'MANIZALES', 3194973890);

insert into Tarjeta (id,numeroTarjeta,ccv,tipoTarjeta,tarjetas)values(1, '1234-5678-9875-3215', 1222, 'MASTER', 1);
insert into Tarjeta (id,numeroTarjeta,ccv,tipoTarjeta,tarjetas)values(2, '1234-5678-9875-4869', 1234, 'CREDITO', 2);
insert into Tarjeta (id,numeroTarjeta,ccv,tipoTarjeta,tarjetas)values(3, '1234-5678-9875-7854', 1456, 'CREDI AGIL', 2);

insert into Historial (id,fechaConsumo,descripcion,monto,consumos)values(1, '2019-03-07','Datos pre cargados 1',2000, 1);
insert into Historial (id,fechaConsumo,descripcion,monto,consumos)values(2, '2019-01-06','Datos pre cargados 2',3000, 1);
insert into Historial (id,fechaConsumo,descripcion,monto,consumos)values(3, '2019-10-09','Datos pre cargados 3',4000,1);

insert into Historial (id,fechaConsumo,descripcion,monto,consumos)values(4, '2019-11-09','Datos consumo 1',4000,2);

insert into Historial (id,fechaConsumo,descripcion,monto,consumos)values(5, '2019-11-09','Datos consumo 2',4000,3);


