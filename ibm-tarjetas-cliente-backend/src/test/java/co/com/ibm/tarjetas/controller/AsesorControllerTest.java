package co.com.ibm.tarjetas.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class AsesorControllerTest {

	private MockMvc mockMvc;
	
	@InjectMocks
	private AsesoresController asesoresController;
	
	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(asesoresController).build();
	}

	@Test
	public void testAdd() throws Exception {		
		mockMvc.perform(post("/api/asesores"));		
	}

}
