package co.com.ibm.tarjetas.service;

import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.ibm.tarjetas.ApiApplicationTests;
import co.com.ibm.tarjetas.mapper.ClienteMapper;
import co.com.ibm.tarjetas.mapper.TarjetaMapper;
import co.com.ibm.tarjetas.model.Cliente;
import co.com.ibm.tarjetas.model.Tarjeta;
import co.com.ibm.tarjetas.utils.Mocks;

public class ClienteTest extends ApiApplicationTests {
	
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	ClienteMapper clienteMapper;
	 
	@Autowired
	Mocks mocks;

	@Test
	public void saveOrUpdateTest() {
		List<Cliente> clientes = mocks.getClientes();		
		clienteService.saveOrUpdate(clientes.get(0));		
	}
	
	@Test
	public void getAllClientNotNullTest() {				
		Set<Cliente> rs = clienteService.getAllClient();	
		assertNotNull(rs);
	}
	
	@Test
	public void getClientByIdNotNullTest() throws JsonProcessingException {
		List<Cliente> clientes = mocks.getClientes();
		Cliente c = clientes.get(0);
		Cliente rs = clienteService.getClientById(c.getId());	
		assertNotNull(rs);
	}
	
	@Test
	public void mapperClientTest() {		
		List<Cliente> clientes = mocks.getClientes();
		Set<Tarjeta> tarjetas = new HashSet<Tarjeta>();
		TarjetaMapper mock = org.mockito.Mockito.mock(TarjetaMapper.class);
		Mockito.when(mock.mapper(tarjetas)).thenReturn(tarjetas);
		Cliente rs = clienteMapper.mapper(clientes.get(0));
		assertNotNull(rs);
	}

}
