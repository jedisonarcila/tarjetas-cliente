package co.com.ibm.tarjetas.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import co.com.ibm.tarjetas.model.Asesor;
import co.com.ibm.tarjetas.model.Cliente;
import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.model.Tarjeta;


@PropertySource("classpath:/mocks.properties")
@Component("mocks")
public class Mocks {
	
	@Value("${ibm.asesores}")
	String asesores;
	
	@Value("${ibm.historiales}")
	String historiales;
	
	@Value("${ibm.clientes}")
	String clientes;
	
	@Value("${ibm.tarjetas}")
	String tarjetas;

	/**
	 * @return the clients
	 */
	public List<Cliente> getClientes() {
		Type listType = new TypeToken<ArrayList<Cliente>>(){}.getType();
		return new Gson().fromJson(clientes, listType);
	}
	
	/**
	 * @return the clients
	 */
	public Set<Cliente> getClientesSet() {
		Type listType = new TypeToken<HashSet<Cliente>>(){}.getType();
		return new Gson().fromJson(clientes, listType);
	}

	/**
	 * @return the cards
	 */
	public List<Tarjeta> getTarjetas() {
		Type listType = new TypeToken<ArrayList<Tarjeta>>(){}.getType();
		return new Gson().fromJson(tarjetas, listType);
	}
	
	/**
	 * @return the cards
	 */
	public Set<Tarjeta> getTarjetasSet() {
		Type listType = new TypeToken<HashSet<Tarjeta>>(){}.getType();
		return new Gson().fromJson(tarjetas, listType);
	}	
	
	/**
	 * @return the histories
	 */
	public List<Historial> getHistoriales() {
		Type listType = new TypeToken<ArrayList<Historial>>(){}.getType();
		return new Gson().fromJson(historiales, listType);
	}
	
	/**
	 * @return the histories
	 */
	public Set<Historial> getHistorialesSet() {
		Type listType = new TypeToken<HashSet<Historial>>(){}.getType();
		return new Gson().fromJson(historiales, listType);
	}
	
	/**
	 * @return the agents
	 */
	public List<Asesor> getAsesores() {
		Type listType = new TypeToken<ArrayList<Asesor>>(){}.getType();
		return new Gson().fromJson(asesores, listType);
	}	
}

