package co.com.ibm.tarjetas.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.ibm.tarjetas.ApiApplicationTests;
import co.com.ibm.tarjetas.mapper.HistorialMapper;
import co.com.ibm.tarjetas.mapper.TarjetaMapper;
import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.model.Tarjeta;
import co.com.ibm.tarjetas.utils.Mocks;

public class TarjetaTest extends ApiApplicationTests{
	
	@Autowired
	TarjetaService tarjetaService;
	
	@Autowired
	TarjetaMapper tarjetaMapper;
	 
	@Autowired
	Mocks mocks;

	@Test
	public void saveOrUpdateTest() {
		List<Tarjeta> tarjetas = mocks.getTarjetas();		
		tarjetaService.saveOrUpdate(tarjetas.get(0));		
	}
	
	@Test
	public void getAllTarjetaNotNullTest() {				
		Set<Tarjeta> rs = tarjetaService.getAllCards();	
		assertNotNull(rs);
	}
	
	@Test
	public void getTarjetaByIdNotNullTest() throws JsonProcessingException {
		List<Tarjeta> tarjetas = mocks.getTarjetas();
		Tarjeta c = tarjetas.get(0);
		Tarjeta rs = tarjetaService.getCardById(c.getId());	
		assertNotNull(rs);
	}
	
	@Test
	public void mapperTarjetaTest() {		
		List<Tarjeta> tarjetas = mocks.getTarjetas();
		Set<Historial> historial = mocks.getHistorialesSet();
		HistorialMapper mock = org.mockito.Mockito.mock(HistorialMapper.class);
		Mockito.when(mock.mapper(historial)).thenReturn(historial);
		Tarjeta rs = tarjetaMapper.mapper(tarjetas.get(0));
		assertNotNull(rs);
	}


}
