package co.com.ibm.tarjetas.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.ibm.tarjetas.ApiApplicationTests;
import co.com.ibm.tarjetas.mapper.AsesorMapper;
import co.com.ibm.tarjetas.model.Asesor;
import co.com.ibm.tarjetas.utils.Mocks;

public class AsesorTest extends ApiApplicationTests {
	
	@Autowired
	AsesorService asesorService;
	
	@Autowired
	AsesorMapper asesorMapper;
	 
	@Autowired
	Mocks mocks;

	@Test
	public void saveOrUpdateTest() {
		List<Asesor> asesores = mocks.getAsesores();
		asesorService.saveOrUpdate(asesores.get(0));		
	}
	
	@Test
	public void getAllClientNotNullTest() {				
		Set<Asesor> rs = asesorService.getAllAgent();
		assertNotNull(rs);
	}
	
	@Test
	public void getClientByIdNotNullTest() throws JsonProcessingException {
		List<Asesor> asesores = mocks.getAsesores();
		Asesor c = asesores.get(0);
		Asesor rs = asesorService.getAgentById(c.getId());	
		assertNotNull(rs);
	}
	
	@Test
	public void mapperClientTest() {		
		List<Asesor> asesores = mocks.getAsesores();
		Asesor rs = asesorMapper.mapper(asesores.get(0));
		assertNotNull(rs);
	}

}
