package co.com.ibm.tarjetas.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.ibm.tarjetas.ApiApplicationTests;
import co.com.ibm.tarjetas.mapper.HistorialMapper;
import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.utils.Mocks;

public class HistorialTest extends ApiApplicationTests{
	
	@Autowired
	HistorialService historialService;
	
	@Autowired
	HistorialMapper historialMapper;
	 
	@Autowired
	Mocks mocks;

	@Test
	public void saveOrUpdateTest() {
		List<Historial> historial = mocks.getHistoriales();	
		historialService.saveOrUpdate(historial.get(0));		
	}
	
	@Test
	public void getAllTarjetaNotNullTest() {				
		Set<Historial> rs = historialService.getAllAgent();
		assertNotNull(rs);
	}
	
	@Test
	public void getTarjetaByIdNotNullTest() throws JsonProcessingException {
		List<Historial> historial = mocks.getHistoriales();
		Historial c = historial.get(0);
		Historial rs = historialService.getAgentById(c.getId());	
		assertNotNull(rs);
	}
	
	@Test
	public void mapperTarjetaTest() {		
		List<Historial> historial = mocks.getHistoriales();
		Historial rs = historialMapper.mapper(historial.get(0));
		assertNotNull(rs);
	}

}
