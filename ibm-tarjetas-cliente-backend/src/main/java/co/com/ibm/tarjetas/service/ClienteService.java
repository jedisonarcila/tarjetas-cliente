package co.com.ibm.tarjetas.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.ibm.tarjetas.mapper.ClienteMapper;
import co.com.ibm.tarjetas.mapper.TarjetaMapper;
import co.com.ibm.tarjetas.model.Cliente;
import co.com.ibm.tarjetas.model.Tarjeta;
import co.com.ibm.tarjetas.repositories.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
    ClienteRepository clienteRepository;
	
	@Autowired
	TarjetaMapper tarjetaMapper;
	
	@Autowired
	ClienteMapper clienteMapper;

	/**
	 * Retorna la lista de todos los clientes
	 * 
	 * @return
	 */
    public Set<Cliente> getAllClient() {
        Set<Cliente> clientes = new HashSet<Cliente>();        
        clienteRepository.findAll().forEach(cliente -> clientes.add(cliente));       
        return clienteMapper.mapper(clientes);
    }

    /**
     * Retorna el cliente que coincida con el id
     * 
     * @param id
     * @return
     * @throws JsonProcessingException
     */
    public Cliente getClientById(int id) throws JsonProcessingException {
    	Cliente cliente = clienteRepository.findById(id).get();
        Set<Tarjeta> tarjetas = tarjetaMapper.mapper(cliente.getTarjetas());
        cliente.setTarjetas(tarjetas);
        return clienteMapper.mapper(cliente);
    }

    /**
     * Guarda o actualiza un cliente
     * 
     * @param cliente
     */
    public void saveOrUpdate(Cliente cliente) {    	
    	clienteRepository.save(cliente);    	
    }

    /**
     * Elimina un cliente
     * 
     * @param id
     */
    public void delete(int id) {
        clienteRepository.deleteById(id);
    }
}
