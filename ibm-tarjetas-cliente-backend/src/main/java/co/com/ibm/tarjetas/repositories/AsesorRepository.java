package co.com.ibm.tarjetas.repositories;

import org.springframework.data.repository.CrudRepository;

import co.com.ibm.tarjetas.model.Asesor;

public interface AsesorRepository extends CrudRepository<Asesor, Integer> {}
