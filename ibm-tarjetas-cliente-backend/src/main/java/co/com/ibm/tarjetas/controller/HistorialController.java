package co.com.ibm.tarjetas.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.service.HistorialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(tags = {"Historial"}, value = "Servicio de historial")
public class HistorialController {
	@Autowired
	HistorialService historialService;

    @GetMapping("/historiales")
    @ApiOperation(value="Consulta todas los historiales", notes = "Consulta todas los historiales")
    private Set<Historial> getAllHistorial() {
        return historialService.getAllAgent();
    }

    @GetMapping("/historiales/{id}")
    private Historial getHistorial(@PathVariable("id") int id) {
        return historialService.getAgentById(id);
    }

    @DeleteMapping("/historiales/{id}")
    private void deleteHistorial(@PathVariable("id") int id) {
    	historialService.delete(id);
    }

    @PostMapping(path="/historiales")
    @ApiOperation(value="Agregar un historial", notes = "Agregar un historial")
    private int saveHistorial(@RequestBody Historial historial) {    	
    	historialService.saveOrUpdate(historial);        
        return historial.getId();
    }
    
    @PutMapping(path="/historiales")
    @ApiOperation(value="Actualiza un historial", notes = "Actualiza un historial")
    private int updateHistorial(@RequestBody Historial historial) {    	
    	historialService.saveOrUpdate(historial);        
        return historial.getId();
    }
}
