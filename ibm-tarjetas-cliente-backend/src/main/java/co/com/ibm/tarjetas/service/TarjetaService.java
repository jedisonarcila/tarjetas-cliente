package co.com.ibm.tarjetas.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.mapper.HistorialMapper;
import co.com.ibm.tarjetas.mapper.TarjetaMapper;
import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.model.Tarjeta;
import co.com.ibm.tarjetas.repositories.TarjetaRepository;

@Service
public class TarjetaService {
	@Autowired
    TarjetaRepository tarjetaRepository;
	
	@Autowired
	TarjetaMapper tarjetaMapper;
	
	@Autowired
	HistorialMapper historialMapper;

	/**
	 * Obtiene todas las tarjetas
	 * 
	 * @return
	 */
    public Set<Tarjeta> getAllCards() {
        Set<Tarjeta> tarjetas = new HashSet<Tarjeta>();
        tarjetaRepository.findAll().forEach(tarjeta -> tarjetas.add(tarjeta));
        return tarjetaMapper.mapper(tarjetas);
    }

    /**
     * Obtiene una tarjeta filtrada por id
     * 
     * @param id
     * @return
     */
    public Tarjeta getCardById(int id) {
    	Tarjeta tarjeta = tarjetaRepository.findById(id).get();
        Set<Historial> consumos = historialMapper.mapper(tarjeta.getConsumos());
        tarjeta.setConsumos(consumos);
        return tarjetaMapper.mapper(tarjeta);       
    }

    /**
     * Guarda o actualiza la tarjeta enviadad como parametro
     * 
     * @param tarjeta
     */
    public void saveOrUpdate(Tarjeta tarjeta) {
    	tarjetaRepository.save(tarjeta);
    }

    /**
     * Elimina la tarjeta que coincida con el id
     * 
     * @param id
     */
    public void delete(int id) {
    	tarjetaRepository.deleteById(id);
    }
}
