package co.com.ibm.tarjetas.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.model.Historial;

@Service("historialMapper")
public class HistorialMapper {

	@Autowired
	TarjetaMapper tarjetaMapper;

	/**
	 * Convierte una lista entidades historial a una lista de historial
	 * 
	 * @param clientes
	 * @return
	 */
	public Set<Historial> mapper(Set<Historial> historiales) {
		Set<Historial> rs = new HashSet<Historial>();
		if (historiales != null) {
			for (Historial h : historiales) {
				Historial h2 = mapper(h);
				rs.add(h2);
			}
		}		
		return rs;
	}

	/**
	 * Convierte una entidad historial a un historial
	 * 
	 * @param c
	 * @return
	 */
	public Historial mapper(Historial h) {
		Historial h2 = new Historial();
		h2.setDescripcion(h.getDescripcion());
		h2.setFechaConsumo(h.getFechaConsumo());
		h2.setId(h.getId());
		h2.setMonto(h.getMonto());		
		return h2;
	}
}
