package co.com.ibm.tarjetas.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.mapper.HistorialMapper;
import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.repositories.HistorialRepository;

@Service
public class HistorialService {
	
	@Autowired
	HistorialRepository historialRepository;
	
	@Autowired
	HistorialMapper historialMapper;

	/**
	 * Retorna todos los historiales
	 * 
	 * @return
	 */
    public Set<Historial> getAllAgent() {
        Set<Historial> historiales = new HashSet<Historial>();
        historialRepository.findAll().forEach(historial -> historiales.add(historial));
        return historialMapper.mapper(historiales);
    }

    /**
     * Retorna un historial por id
     * 
     * @param id
     * @return
     */
    public Historial getAgentById(int id) {
    	Historial historial = historialRepository.findById(id).get();
        return historialMapper.mapper(historial);
    }

    /**
     * Guarda o actualiza un historial
     * 
     * @param asesor
     */
    public void saveOrUpdate(Historial historial) {
    	historialRepository.save(historial);
    }

    /**
     * Elimina el historial que coincida con el id
     * 
     * @param id
     */
    public void delete(int id) {
    	historialRepository.deleteById(id);
    }
}
