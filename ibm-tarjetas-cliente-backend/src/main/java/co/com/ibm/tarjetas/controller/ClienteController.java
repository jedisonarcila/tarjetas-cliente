package co.com.ibm.tarjetas.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import co.com.ibm.tarjetas.model.Cliente;
import co.com.ibm.tarjetas.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(tags = {"Cliente"}, value = "Servicio de clientes")
public class ClienteController {
	@Autowired
    ClienteService clienteService;

    @GetMapping("/clientes")
    @ApiOperation(value="Consulta todos los cliente", notes = "Consulta todos los clientes")
    private Set<Cliente> getAllClientes() {
        return clienteService.getAllClient();
    }

    @GetMapping(path = "/clientes/{id}")
    private Cliente getClient(@PathVariable("id") int id) throws JsonProcessingException {
    	Cliente cliente = clienteService.getClientById(id);    	
        return cliente;
    }

    @DeleteMapping("/clientes/{id}")
    private void deleteClient(@PathVariable("id") int id) {
        clienteService.delete(id);
    }

    @PostMapping("/clientes")
    @ApiOperation(value="Adicionar un cliente", notes = "Adicionar un cliente")
    private int saveClient(@RequestBody Cliente cliente) {    	
        clienteService.saveOrUpdate(cliente);        
        return cliente.getId();
    }
    
    @PutMapping("/clientes")
    @ApiOperation(value="Actualiza un cliente", notes = "Actualiza un cliente")
    private int updateClient(@RequestBody Cliente cliente) {    	
        clienteService.saveOrUpdate(cliente);        
        return cliente.getId();
    }
}
