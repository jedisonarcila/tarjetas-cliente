package co.com.ibm.tarjetas.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Tarjeta")
@SequenceGenerator(name="seq", initialValue=50, allocationSize=999999999)
public class Tarjeta {
    
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private int id;
	@NotNull
	@Column(name = "NUMEROTARJETA")
    private String numeroTarjeta;
	@Size(min=3, max=4, message = "Debe tener entre 3 y 4 caracteres")
	@Column(name = "ccv")
    private int ccv;
	@Column(name = "TIPOTARJETA")
    private String tipoTarjeta;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "tarjetas")	
    private Cliente tarjetas;    
    @OneToMany(fetch=FetchType.EAGER)  
    @JoinColumn(name = "consumos")
    private Set<Historial> consumos;   
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the numeroTarjeta
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	/**
	 * @param numeroTarjeta the numeroTarjeta to set
	 */
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	/**
	 * @return the ccv
	 */
	public int getCcv() {
		return ccv;
	}
	/**
	 * @param ccv the ccv to set
	 */
	public void setCcv(int ccv) {
		this.ccv = ccv;
	}
	/**
	 * @return the tipoTarjeta
	 */
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	/**
	 * @param tipoTarjeta the tipoTarjeta to set
	 */
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	/**
	 * @return the cliente
	 */
	public Cliente getTarjeta() {
		return tarjetas;
	}
	/**
	 * @param cliente the tarjeta to set
	 */
	public void setTarjeta(Cliente tarjetas) {
		this.tarjetas = tarjetas;
	}
	/**
	 * @return the consumos
	 */
	public Set<Historial> getConsumos() {
		return consumos;
	}
	/**
	 * @param consumos the consumos to set
	 */
	public void setConsumos(Set<Historial> consumos) {
		this.consumos = consumos;
	}
}

