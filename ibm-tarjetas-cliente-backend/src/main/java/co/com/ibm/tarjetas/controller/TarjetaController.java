package co.com.ibm.tarjetas.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.ibm.tarjetas.model.Tarjeta;
import co.com.ibm.tarjetas.service.TarjetaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(tags = {"Tarjeta"}, value = "Servicio de tarjetas")
public class TarjetaController {
	@Autowired
    TarjetaService tarjetaService;

    @GetMapping("/tarjetas")
    @ApiOperation(value="Consulta todas las tarjetas", notes = "Consulta todas las tarjetas")
    private Set<Tarjeta> getAllCards() {
        return tarjetaService.getAllCards();
    }

    @GetMapping("/tarjetas/{id}")
    private Tarjeta getCard(@PathVariable("id") int id) {
        return tarjetaService.getCardById(id);
    }

    @DeleteMapping("/tarjetas/{id}")
    private void deleteCard(@PathVariable("id") int id) {
    	tarjetaService.delete(id);
    }

    @PostMapping(path="/tarjetas")
    @ApiOperation(value="Adicionar una tarjeta", notes = "Adicionar una tarjeta")
    private int saveCard(@RequestBody Tarjeta tarjeta) {    	
    	tarjetaService.saveOrUpdate(tarjeta);        
        return tarjeta.getId();
    }
    
    @PutMapping(path="/tarjetas")
    @ApiOperation(value="Actualiza una tarjeta", notes = "Actualiza una tarjeta")
    private int updateCard(@RequestBody Tarjeta tarjeta) {    	
    	tarjetaService.saveOrUpdate(tarjeta);        
        return tarjeta.getId();
    }
}
