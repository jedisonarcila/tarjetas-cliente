package co.com.ibm.tarjetas.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Cliente")
@SequenceGenerator(name="seq", initialValue=50, allocationSize=999999999)
public class Cliente {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private int id;
	@Column(name = "nombre", length = 50)
    private String nombre;
	@Column(name = "direccion", length = 100)
    private String direccion;
	@Column(name = "ciudad", length = 30)
    private String ciudad;
	@Column(name = "telefono", length = 20)
    private long telefono;
	@Transient
	private int numeroTarjetas;
    @OneToMany(fetch=FetchType.EAGER)  
    @JoinColumn(name = "tarjetas")   
    private Set<Tarjeta> tarjetas;	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * @param ciudad the ciudad to set
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * @return the telefono
	 */
	public long getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the tarjetas
	 */
	public Set<Tarjeta> getTarjetas() {
		return tarjetas;
	}
	/**
	 * @param tarjetas the tarjetas to set
	 */
	public void setTarjetas(Set<Tarjeta> tarjetas) {
		this.tarjetas = tarjetas;
	}
	/**
	 * @return the numeroTarjetas
	 */
	public int getNumeroTarjetas() {
		return numeroTarjetas;
	}
	/**
	 * @param numeroTarjetas the numeroTarjetas to set
	 */
	public void setNumeroTarjetas(int numeroTarjetas) {
		this.numeroTarjetas = numeroTarjetas;
	}	
	
}
