package co.com.ibm.tarjetas.repositories;

import org.springframework.data.repository.CrudRepository;

import co.com.ibm.tarjetas.model.Historial;

public interface HistorialRepository extends CrudRepository<Historial, Integer> {}
