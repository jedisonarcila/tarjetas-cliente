package co.com.ibm.tarjetas.repositories;

import org.springframework.data.repository.CrudRepository;

import co.com.ibm.tarjetas.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {}
