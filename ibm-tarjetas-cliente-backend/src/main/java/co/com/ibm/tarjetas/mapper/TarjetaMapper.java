package co.com.ibm.tarjetas.mapper;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.model.Historial;
import co.com.ibm.tarjetas.model.Tarjeta;

@Service("tarjetaMapper")
public class TarjetaMapper {
	
	@Autowired
	HistorialMapper historialMapper;

	/**
	 * Convierte una lista set entidades de tarjetas a una lista de tarjetas
	 * 
	 * @param tarjeta
	 * @return
	 */
    public Set<Tarjeta> mapper(Set<Tarjeta> tarjeta) {    	
    	Set<Tarjeta> tarjetas = new HashSet<Tarjeta>();
    	for (Tarjeta t : tarjeta) {
    		Tarjeta t2 = mapper(t);	
			tarjetas.add(t2);
		}
    	return tarjetas;
    }    
    
    /**
     * Convierte mapea una entidad tarjeta a una tarjeta
     * 
     * @param t
     * @return
     */
    public Tarjeta mapper(Tarjeta t) {   	
		Tarjeta t2 = new Tarjeta();
		t2.setId(t.getId());
		t2.setCcv(t.getCcv());
		t2.setConsumos(t.getConsumos());
		
		Set<Historial> h = historialMapper.mapper(t.getConsumos());
		t2.setConsumos(h);
		
		t2.setNumeroTarjeta(t.getNumeroTarjeta());
		t2.setTipoTarjeta(t.getTipoTarjeta());
    	return t2;
    }
}
