package co.com.ibm.tarjetas.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.mapper.AsesorMapper;
import co.com.ibm.tarjetas.model.Asesor;
import co.com.ibm.tarjetas.repositories.AsesorRepository;

@Service
public class AsesorService {
	
	@Autowired
    AsesorRepository asesorRepository;
	
	@Autowired
	AsesorMapper asesorMapper;

	/**
	 * Retorna todos los asesores
	 * 
	 * @return
	 */
    public Set<Asesor> getAllAgent() {
        Set<Asesor> asesores = new HashSet<Asesor>();
        asesorRepository.findAll().forEach(asesor -> asesores.add(asesor));
        return asesorMapper.mapper(asesores);
    }

    /**
     * Retorna un asesor por id
     * 
     * @param id
     * @return
     */
    public Asesor getAgentById(int id) {
    	Asesor asesor = asesorRepository.findById(id).get();
        return asesorMapper.mapper(asesor);
    }

    /**
     * Guarda o actualiza un asesor
     * 
     * @param asesor
     */
    public Asesor saveOrUpdate(Asesor asesor) {
    	return asesorRepository.save(asesor);
    }

    /**
     * Elimina el asesor que coincida con el id
     * 
     * @param id
     */
    public void delete(int id) {
    	asesorRepository.deleteById(id);
    }
}
