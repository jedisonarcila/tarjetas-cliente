package co.com.ibm.tarjetas.mapper;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.model.Asesor;

@Service("asesorMapper")
public class AsesorMapper {

	/**
	 * Convierte una lista set entidades de asesores a una lista de asesores
	 * 
	 * @param asesor
	 * @return
	 */
    public Set<Asesor> mapper(Set<Asesor> asesor) {    	
    	Set<Asesor> asesores = new HashSet<Asesor>();
    	for (Asesor s : asesor) {
    		Asesor s2 = mapper(s);	
    		asesores.add(s2);
		}
    	return asesores;
    }    
    
    /**
     * Convierte mapea una entidad asesor a un asesor
     * 
     * @param s
     * @return
     */
    public Asesor mapper(Asesor s) {   	
    	Asesor s2 = new Asesor();
		s2.setId(s.getId());
		s2.setEspecialidad(s.getEspecialidad());
		s2.setNombre(s.getNombre());
    	return s2;
    }
}
