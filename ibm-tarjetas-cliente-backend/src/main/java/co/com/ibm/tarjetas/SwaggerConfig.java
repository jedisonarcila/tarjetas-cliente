package co.com.ibm.tarjetas;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Habilitando la configuración para swagger
 * 
 * http://localhost:8098/swagger-ui.html
 * http://localhost:8098/v2/api-docs
 * 
 */
@Configuration
@EnableSwagger2
@PropertySource({ "classpath:apidoc.properties"	})
public class SwaggerConfig {

	/**
	 * Tipos de informacion API.
	 */
	private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<>(
			Arrays.asList("application/json"));

	/**
	 * Metadata API
	 * @return
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(metaData())
				.produces(DEFAULT_PRODUCES_AND_CONSUMES)
				.consumes(DEFAULT_PRODUCES_AND_CONSUMES)				
                .select().apis(RequestHandlerSelectors.basePackage("co.com.ibm.tarjetas.controller"))                
                .build();		
	}

	/**
	 * Base Metadata
	 * @return
	 */
	private ApiInfo metaData() {
		return new ApiInfo("Spring Boot REST API", "Spring Boot REST API for Avisos Mantenimientos Proveedores", "1.0",
				"Terms of service", new Contact("Jhon Edison Arcila Ocampo", "http:/localhost/api/", "jarcila@hotmail.com.co"),
				"Apache License Version 2.0", "https://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
	}
	
}