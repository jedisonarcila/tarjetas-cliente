package co.com.ibm.tarjetas.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.ibm.tarjetas.model.Asesor;
import co.com.ibm.tarjetas.service.AsesorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(tags = {"Asesor"}, value = "Servicio de asesores")
public class AsesoresController {
	@Autowired
    AsesorService asesorService;

    @GetMapping("/asesores")
    @ApiOperation(value="Consulta todos los asesores", notes = "Consulta todos los asesores")
    private Set<Asesor> getAllAsesores() {
        return asesorService.getAllAgent();
    }

    @GetMapping("/asesores/{id}")
    private Asesor getAsesor(@PathVariable("id") int id) {
        return asesorService.getAgentById(id);
    }

    @DeleteMapping("/asesores/{id}")
    private void deleteAsesor(@PathVariable("id") int id) {
    	asesorService.delete(id);
    }

    @PostMapping("/asesores")
    @ApiOperation(value="Agregar un asesor", notes = "Agregar un asesor")
    private int saveAsesor(@RequestBody Asesor asesor) {    	
    	asesorService.saveOrUpdate(asesor);        
        return asesor.getId();
    }
    
    @PutMapping("/asesores")
    @ApiOperation(value="Actualiza un asesor", notes = "Actualiza un asesor")
    private int updateAsesor(@RequestBody Asesor asesor) {    	
    	asesorService.saveOrUpdate(asesor);        
        return asesor.getId();
    }
}
