package co.com.ibm.tarjetas.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Historial")
@SequenceGenerator(name="seq", initialValue=50, allocationSize=999999999)
public class Historial {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private int id;
	@Column(name="FECHACONSUMO")
    private Date fechaConsumo;
    private String descripcion;
    private long monto;
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "consumos")
    private Tarjeta consumos;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the fechaConsumo
	 */
	public Date getFechaConsumo() {
		return fechaConsumo;
	}
	/**
	 * @param fechaConsumo the fechaConsumo to set
	 */
	public void setFechaConsumo(Date fechaConsumo) {
		this.fechaConsumo = fechaConsumo;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the monto
	 */
	public long getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(long monto) {
		this.monto = monto;
	}
	/**
	 * @return the tarjeta
	 */
	public Tarjeta getTarjeta() {
		return consumos;
	}
	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(Tarjeta tarjeta) {
		this.consumos = tarjeta;
	}	
}

