package co.com.ibm.tarjetas.repositories;

import org.springframework.data.repository.CrudRepository;

import co.com.ibm.tarjetas.model.Tarjeta;

public interface TarjetaRepository extends CrudRepository<Tarjeta, Integer> {}
