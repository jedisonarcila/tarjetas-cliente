package co.com.ibm.tarjetas.mapper;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ibm.tarjetas.model.Cliente;
import co.com.ibm.tarjetas.model.Tarjeta;

@Service("clienteMapper")
public class ClienteMapper {
	
	@Autowired
	TarjetaMapper tarjetaMapper;
	
	/**
	 * Convierte una lista entidades cliente a una lista de clientes
	 * 
	 * @param clientes
	 * @return
	 */
	public Set<Cliente> mapper(Set<Cliente> clientes) {
		Set<Cliente> rs = new HashSet<>();
    	for (Cliente c : clientes) {
    		 Cliente c2 = mapper(c);
    		 rs.add(c2);
		}
    	return rs;
    }
	
	/**
	 * Convierte una entidad cliente a un cliente
	 * 
	 * @param c
	 * @return
	 */
	public Cliente mapper(Cliente c) {		
		 Cliente c2 = new Cliente();
		 c2.setCiudad(c.getCiudad());
		 c2.setDireccion(c.getDireccion());
		 c2.setId(c.getId());
		 c2.setNombre(c.getNombre());
		 c2.setTelefono(c.getTelefono());	
		 Set<Tarjeta> t = tarjetaMapper.mapper(c.getTarjetas());		 
		 int num = t != null ? t.size() : 0;
		 c2.setNumeroTarjetas(num);
		 c2.setTarjetas(t);    	
    	return c2;
    }
	
}
